package hu.bme.medialab1.videomessenger;

import java.io.*;
import java.net.*;
import java.nio.charset.Charset;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A dummy fragment representing a section of the app, but that simply
 * displays dummy text.
 */
public class DummySectionFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
	private View rootView;
    private int ARG_SECTION = 0;
    
    public static void toast(Context context, String message) {
		// this is a static method so it is easier to call,
		// as the context checking and casting is done

		if (context == null) return;

		if (context instanceof Activity) {
			final Context c = context;
			final String m = message;

			((Activity)context).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(c, m, Toast.LENGTH_LONG).show();
				}
			});
		}
	}
    
    View.OnClickListener but_save_handler = new View.OnClickListener() {
        public void onClick(View v) {
          SharedPreferences.Editor edit = ((MainActivity)getActivity()).preferences.edit();
          EditText ip = (EditText) rootView.findViewById(R.id.editIPAddr);
          edit.putString("ip_addr", ip.getText().toString());
          edit.apply();
        }
      };
    View.OnClickListener connect_handler = new View.OnClickListener() {
          public void onClick(View v) {
              final String serverIpAddress = ((MainActivity)getActivity()).preferences.getString("ip_addr", "0.0.0.0");
              new Thread(new Runnable() { //We can't do networking in main thread
	              public void run() {
	            	  if ( ((MainActivity)getActivity()).socket != null) {
	            		  try {
	            			  ((MainActivity)getActivity()).socket.close();
	            		  } catch (Exception e) {}
	            	  }
	            	  try {
	                 InetAddress serverAddr = InetAddress.getByName(serverIpAddress);
	                 ((MainActivity)getActivity()).socket = new Socket(serverAddr, 4407);
	                 ((MainActivity)getActivity()).socket.setKeepAlive(true);
	              } catch (UnknownHostException e1) {
	              	toast(getActivity(), "Connect: Unknown host: "+serverIpAddress);
	              } catch (IOException e1) {
	              	toast(getActivity(), "Connect: I/O Error");
	              }
	              }
              }).start();
          }
        };
        
    View.OnClickListener but_send_handler = new View.OnClickListener() {
            public void onClick(View v) {
            	final TextView tv = (TextView) rootView.findViewById(R.id.editText1);
            	Socket socket = ((MainActivity)getActivity()).socket;
            	if ( socket == null || socket.isClosed() ) { //isClosed will only return true if we explicitly closed the connection...
            		new Thread(new Runnable() {
            			public void run() {
            				Looper.prepare();
            				Looper.loop();
            				toast(getActivity(), "No connection. Trying to connect automatically...");
            				Looper.getMainLooper().quit();
            			}
            		}).start();
            		
            		connect_handler.onClick(rootView);
            		for (int i=0; i<25; ++i) { //Max 5 sec wait
            			socket = ((MainActivity)getActivity()).socket;
            			if ( socket == null || !socket.isConnected() ) {
			            	try {
			            		Thread.sleep(200);
			            	} catch ( Exception e ) {}
            			} else break;
            		}
            	}
            	if ( socket != null && socket.isConnected() ) {
            		boolean redo=false;
            		final boolean redosafe;
	            	try {
	                     String str = tv.getText().toString();
	                     PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(),Charset.forName("UTF-8"))),true);
	                     out.println(str+"|");
	                     tv.setText("");
	                  } catch (Exception e) {
	                	  //toast(getActivity(), "Error: "+e.getMessage());
	                	  redo=true;
	                  }
	            	finally {
	            		redosafe=redo;
	                   	 	new Thread(new Runnable() {
	                   	 		public void run() {
		                   	 		for (int i=0; i<25; ++i) { //Max 5 sec wait to close
		                    			Socket socket = ((MainActivity)getActivity()).socket;
		                    			if ( !socket.isClosed() ) {
		        			            	try {
		        			            		socket.close(); //Close the socket, so we will reconnect on next message. This is the most stable way.
		        			            		Thread.sleep(200);
		        			            	} catch ( Exception e ) {}
		                    			} else break;
		                    		}
		                   	 		((MainActivity)getActivity()).socket = null;
		                   	 		if (redosafe) getActivity().runOnUiThread(new Runnable() {
		                       	 		public void run() {
		                       	 			but_send_handler.onClick(rootView);
		                       	 		}
		                       	 	});
	                   	 		}
	                   	 	}).start();
	            	}
            	}
            	else {
            		toast(getActivity(), "Failed! Please connect manually.");
            	}
            }
          };

    public DummySectionFragment() {
    }
    
    public void set_section(int section) {
    	ARG_SECTION = section;
    }
    
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
      super.onSaveInstanceState(savedInstanceState);
      // Save UI state changes to the savedInstanceState.
      // This bundle will be passed to onCreate if the process is
      // killed and restarted.
      savedInstanceState.putInt("ARG_SECTION", ARG_SECTION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);
    	int resource;
    	if ( savedInstanceState != null && savedInstanceState.containsKey("ARG_SECTION"))
    		ARG_SECTION=savedInstanceState.getInt("ARG_SECTION");
    	switch ( ARG_SECTION ) {
    		case 0:
	    		resource=R.layout.fragment_main;
	    		break;
        	case 1:
        		resource=R.layout.fragment_settings;
        		break;
        	case 2:
        		resource=R.layout.fragment_about;
        		break;
        	default:
        		resource=R.layout.fragment_main_dummy;
    	}
        rootView = inflater.inflate(resource, container, false);
        if ( ARG_SECTION == 0) {
        	Button bt = (Button) rootView.findViewById(R.id.but_send);
            bt.setOnClickListener(but_send_handler);
        }
        else if ( ARG_SECTION == 1 ) {
        	EditText ip = (EditText) rootView.findViewById(R.id.editIPAddr);
    		ip.setText(((MainActivity)getActivity()).preferences.getString("ip_addr", "0.0.0.0"));
    		Button but_save = (Button) rootView.findViewById(R.id.but_saveIP);
    		Button but_connect = (Button) rootView.findViewById(R.id.but_connect);
    		but_save.setOnClickListener(but_save_handler);
    		but_connect.setOnClickListener(connect_handler);
        }
        return rootView;
    }
    
}
